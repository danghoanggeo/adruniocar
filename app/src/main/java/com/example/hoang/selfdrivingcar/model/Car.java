package com.example.hoang.selfdrivingcar.model;

/**
 * Created by hoang on 8/18/2017.
 */

public class Car {
    public static final String MOVE_STATUS_GO = "G"; // Go
    public static final String MOVE_STATUS_BA = "B";  // back
    public static final String MOVE_STATUS_GORI = "R"; // Go and turn right
    public static final String MOVE_STATUS_GOLE = "L"; // Go and tunr left
    public static final String MOVE_STATUS_BARI = "W";  // Back and turn right
    public static final String MOVE_STATUS_BALE = "E"; // Back and turn left
    public static final String MOVE_STATUS_STOP = "T"; // Stop
    public static final String CONTROL_STATUS_A = "A"; // Auto
    public static final String CONTROL_STATUS_D = "D"; // Hand device
    public static final String CONTROL_STATUS_V = "V"; // voice
    private String goStatus;
    private String controlStatus;
    private int speed = 0;
    public Car(){
        goStatus = MOVE_STATUS_STOP;
        controlStatus = CONTROL_STATUS_D;
        speed = 120;
    }

    public String getGoStatus() {
        return goStatus;
    }

    public void setGoStatus(String goStatus) {
        this.goStatus = goStatus;
    }

    public String getControlStatus() {
        return controlStatus;
    }

    public void setControlStatus(String controlStatus) {
        this.controlStatus = controlStatus;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }
}
