package com.example.hoang.selfdrivingcar;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
;
import java.util.ArrayList;
import java.util.Set;
import java.util.UUID;

public class BluetoothConActivity extends AppCompatActivity {
    TextView txvNotification;
    ListView lsvDevicelist;
    String address = null;
    private BluetoothAdapter mBluetoothAdapter;
    private Set<BluetoothDevice> pairedDevices;
    private boolean isEnableBlue = false;
    private final static int REQUEST_ENABLE_BT = 1; // used to identify adding bluetooth names
    public static String EXTRA_ADDRESS = "device_address";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bluetooth_con);
        init();
    }

    private void init()
    {
        txvNotification = (TextView)findViewById(R.id.txvNotification);
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        lsvDevicelist = (ListView)findViewById(R.id.lsvDevicelist);
        if (mBluetoothAdapter == null) {
            msg("Device doesn't support Bluetooth chanel!");
            Intent mainActivity = new Intent(BluetoothConActivity.this,MainActivity.class);
            startActivity(mainActivity);
        }
        else
        {
            if (!mBluetoothAdapter.isEnabled()) {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
                if(isEnableBlue)
                {
                    pairedDevices();
                }
            }
            else{
                pairedDevices();
            }
        }
    }
    public void pairedDevices()
    {
        pairedDevices = mBluetoothAdapter.getBondedDevices();
        if (pairedDevices.size() > 0) {
            txvNotification.setText("Connecting to your car via bluetooth.\n please click to connect!");
            ArrayList list = new ArrayList();
            // There are paired devices. Get the name and address of each paired device.
            for (BluetoothDevice device : pairedDevices) {
                list.add(device.getName() + "\n" + device.getAddress()); //Get the device's name and the address
            }
            final ArrayAdapter adapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1, list);
            lsvDevicelist.setAdapter(adapter);
            lsvDevicelist.setOnItemClickListener(myListClickListener); //Method called when the device from the list is clicked
        }
        else
        {
            txvNotification.setText("No Paired Bluetooth Devices Found.");
            msg("No Paired Bluetooth Devices Found.");
        }
    }
    private AdapterView.OnItemClickListener myListClickListener = new AdapterView.OnItemClickListener()
    {
        public void onItemClick (AdapterView<?> av, View v, int arg2, long arg3)
        {
            // Get the device MAC address, the last 17 chars in the View
            String info = ((TextView) v).getText().toString();
            address = info.substring(info.length() - 17);
            Intent mainActivity = new Intent(BluetoothConActivity.this,MainActivity.class);
            mainActivity.putExtra(EXTRA_ADDRESS,address);
            startActivity(mainActivity);
        }
    };
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_ENABLE_BT) {
            if (resultCode == RESULT_OK) {
                isEnableBlue = true;
                msg("Bluetooth chanel is opening");
            } else if (resultCode == RESULT_CANCELED) {
                isEnableBlue = false;
                msg("Bluetooth chanel is closed");
            }
        }
    }

    private void msg(String msg)
    {
        Toast.makeText(getApplicationContext(),msg,Toast.LENGTH_LONG).show();
    }

}
