package com.example.hoang.selfdrivingcar;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.speech.RecognizerIntent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hoang.selfdrivingcar.controler.BluetoothService;
import com.example.hoang.selfdrivingcar.model.Car;

import java.util.ArrayList;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity {
    String addressDevice;
    SeekBar sbSpeed;
    ImageView imgGo,imgBack,imgLeft,imgRight,imgStop
            ,imgHandControl,imgAutorun,imgDisconnect
            ,imgVoiceControl;
    TextView txvVoiceControl;
    BluetoothService bluetoothService;
    Car myCar;
    private final int REQ_CODE_SPEECH_INPUT = 100;
    short speed  = 100;
    String textReconiz="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }
    private void init()
    {
        myCar = new Car();
        Intent newIntent = getIntent();
        if(newIntent != null)
        {
            addressDevice = newIntent.getStringExtra(BluetoothConActivity.EXTRA_ADDRESS);
            bluetoothService = new BluetoothService(addressDevice);
            conectBluetooth();
        }
        imgDisconnect = (ImageView) findViewById(R.id.imgDisconnect);
        imgGo = (ImageView)findViewById(R.id.imgGo);
        imgBack = (ImageView)findViewById(R.id.imgBack);
        imgLeft = (ImageView)findViewById(R.id.imgLeft);
        imgRight = (ImageView)findViewById(R.id.imgRight);
        imgStop = (ImageView)findViewById(R.id.imgStop);
        imgHandControl = (ImageView)findViewById(R.id.imgHandControl);
        imgAutorun = (ImageView)findViewById(R.id.imgAutorun);
        imgVoiceControl = (ImageView)findViewById(R.id.imgVoiceControl);
        txvVoiceControl = (TextView)findViewById(R.id.txvVoiceControl);
        sbSpeed = (SeekBar)findViewById(R.id.sbSpeed);
        sbSpeed.setMax(255);
        sbSpeed.setProgress(myCar.getSpeed());
        controlAction();
    }
    private void controlAction()
    {
        imgAutorun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myCar.setControlStatus(myCar.CONTROL_STATUS_A);
                setDisableHandControl();
                sendControlToCar();
                msg("Automatic run");
            }
        });
        imgHandControl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myCar.setControlStatus(myCar.CONTROL_STATUS_D);
                setEnableHandControl();
                sendControlToCar();
                controlByHand();
                msg("Hand control");
            }
        });
        imgVoiceControl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myCar.setControlStatus(myCar.CONTROL_STATUS_D);
                controlByVoice();
            }
        });
        imgDisconnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bluetoothService.cancel();
                Intent BluetoothCon = new Intent(MainActivity.this,BluetoothConActivity.class);
                startActivity(BluetoothCon);
            }
        });


        sbSpeed.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                myCar.setSpeed(sbSpeed.getProgress());
                sendSpeedToCar();
            }
        });

    }

    private void setEnableHandControl() {
        imgGo.setVisibility(View.VISIBLE);
        imgBack.setVisibility(View.VISIBLE);
        imgStop.setVisibility(View.VISIBLE);
        imgLeft.setVisibility(View.VISIBLE);
        imgRight.setVisibility(View.VISIBLE);
    }

    private void setDisableHandControl() {
        imgGo.setVisibility(View.GONE);
        imgBack.setVisibility(View.GONE);
        imgStop.setVisibility(View.GONE);
        imgLeft.setVisibility(View.GONE);
        imgRight.setVisibility(View.GONE);
    }


    private void controlByVoice(){
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                getString(R.string.speech_prompt));
        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
        } catch (ActivityNotFoundException a) {
            Toast.makeText(getApplicationContext(),
                    getString(R.string.speech_not_supported),
                    Toast.LENGTH_SHORT).show();
        }
    }
    /**
     * Receiving speech input
     * */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQ_CODE_SPEECH_INPUT: {
                if (resultCode == RESULT_OK && null != data) {

                    ArrayList<String> result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    textReconiz = result.get(0);
                    txvVoiceControl.setText(result.get(0));
                }
                setCarStatusByVoiceData();
                break;
            }

        }

    }

    private void setCarStatusByVoiceData()
    {
        if(textReconiz.matches(".*(Go|Move|Đi|thẳng).*")){
            myCar.setGoStatus(myCar.MOVE_STATUS_GO);
        }
        else if(textReconiz.matches(".*(Stop|stop|Dùng|dừng lại).*"))
        {
            myCar.setGoStatus(myCar.MOVE_STATUS_STOP);
        }
        else if(textReconiz.matches(".*(Go and turn right|go and turn right|Đi và rẽ phải|đi và rẽ phải).*"))
        {
            myCar.setGoStatus(myCar.MOVE_STATUS_GORI);
        }
        else if(textReconiz.matches(".*(Go and turn left|go and turn left|Đi và rẽ trái|đi và rẽ trái).*"))
        {
            myCar.setGoStatus(myCar.MOVE_STATUS_GOLE);
        }
        else if(textReconiz.matches(".*(Back and turn right|back and turn right|Lùi và rẽ phải|lùi và rẽ phải).*"))
        {
            myCar.setGoStatus(myCar.MOVE_STATUS_BARI);
        }
        else if(textReconiz.matches(".*(Back and turn left|back and turn left|Lùi và rẽ trái|lùi và rẽ trái).*"))
        {
            myCar.setGoStatus(myCar.MOVE_STATUS_BALE);
        }
        else if(textReconiz.matches(".*(Back|Come back|Lùi).*"))
        {
            myCar.setGoStatus(myCar.MOVE_STATUS_BA);
        }
        else if(textReconiz.matches(".*(Speed up|speed up|Tăng tốc|tăng tốc).*"))
        {
            if(myCar.getSpeed()<=200) {
                myCar.setSpeed(myCar.getSpeed() + 50);
                sendSpeedToCar();
            }
            else{
                msg("Max speed! Speed Can't up.");
            }
        }
        else if(textReconiz.matches(".*(Speed down|speed down|Giảm tốc|giảm tốc).*"))
        {
            if(myCar.getSpeed()>55){
                myCar.setSpeed(myCar.getSpeed()-50);
                sendSpeedToCar();
            }else{
                msg("Min speed! Speed Can't down.");
            }
        }
        sendActionToCar();
    }
    private void sendControlToCar()
    {
        sendDataToCar(myCar.getControlStatus());
    }
    private void sendActionToCar()
    {
        sendDataToCar(myCar.getGoStatus());
    }
    private void sendSpeedToCar()
    {
        sendDataToCar(String.valueOf(myCar.getSpeed()));
        sbSpeed.setProgress(myCar.getSpeed());
    }
    private void controlByHand()
    {

        imgGo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(myCar.getControlStatus().equals(myCar.CONTROL_STATUS_D))
                {
                    myCar.setGoStatus(myCar.MOVE_STATUS_GO);
                    sendActionToCar();
                }
            }
        });
        imgGo.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                speed += 40;
                sbSpeed.setProgress(speed);
                return true;
            }
        });

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(myCar.getControlStatus().equals(myCar.CONTROL_STATUS_D))
                {
                    myCar.setGoStatus(myCar.MOVE_STATUS_BA);
                    sendActionToCar();
                }
            }
        });
        imgRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(myCar.getControlStatus().equals(myCar.CONTROL_STATUS_D))
                {
                    if(myCar.getGoStatus().equals(myCar.MOVE_STATUS_GO))
                    {
                        myCar.setGoStatus(myCar.MOVE_STATUS_GORI);
                    }
                    else if(myCar.getGoStatus().equals(myCar.MOVE_STATUS_BA))
                    {
                        myCar.setGoStatus(myCar.MOVE_STATUS_BARI);
                    }
                    else{
                        myCar.setGoStatus(myCar.MOVE_STATUS_GORI);
                    }
                    sendActionToCar();
                }

            }
        });
        imgLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(myCar.getControlStatus().equals(myCar.CONTROL_STATUS_D))
                {
                    if(myCar.getGoStatus().equals(myCar.MOVE_STATUS_GO))
                    {
                        myCar.setGoStatus(myCar.MOVE_STATUS_GOLE);
                    }
                    else if(myCar.getGoStatus().equals(myCar.MOVE_STATUS_BA))
                    {
                        myCar.setGoStatus(myCar.MOVE_STATUS_BALE);
                    }else{
                        myCar.setGoStatus(myCar.MOVE_STATUS_GOLE);
                    }
                    sendActionToCar();
                }
            }
        });
        imgStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(myCar.getControlStatus().equals(myCar.CONTROL_STATUS_D))
                {
                    myCar.setGoStatus(myCar.MOVE_STATUS_STOP);
                    sendActionToCar();
                }
            }
        });
    }
    private void sendDataToCar(String action)
    {
        bluetoothService.write(action.getBytes());
    }
    private void conectBluetooth()
    {
        if(!(addressDevice == null))
        {
            bluetoothService.run();
            msg("Connected!");
        }
        else
        {
            msg("Can't connect to : "+addressDevice);
        }
    }

    private void msg(String msg)
    {
        Toast.makeText(getApplicationContext(),msg,Toast.LENGTH_LONG).show();
    }
}
