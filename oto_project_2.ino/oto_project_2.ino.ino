#include <SoftwareSerial.h>
#include <String.h>
// Motor
#define IN1	7
#define IN2	6
#define IN3	5
#define IN4	4
// Do khoang cach SRO4
#define TRIGPIN 9
#define ECHOPIN 8
// LS
#define SENSORPIN 0
#define LEDPIN 13
// Bluetooth
#define RX_PIN 2
#define TX_PIN 3
#define MAX_SPEED 255 //từ 0-255
#define MIN_SPEED 0
const char MOVE_STATUS_GO = 'G';
const char MOVE_STATUS_BA = 'B';
const char MOVE_STATUS_GORI = 'R';
const char MOVE_STATUS_GOLE = 'L';
const char MOVE_STATUS_BARI = 'W';
const char MOVE_STATUS_BALE = 'E';
const char MOVE_STATUS_STOP = 'T';
const char CONTROL_STATUS_A = 'A';
const char CONTROL_STATUS_D = 'D';
char CONTROL_STATUS = CONTROL_STATUS_A;
char GO_STATUS = MOVE_STATUS_GO;
String Speeds = "";
int Speedi = 100;
SoftwareSerial serial(RX_PIN,TX_PIN);
void setup()
{
        Serial.begin(9600);
        serial.begin(38400);
        // Motor
	pinMode(IN1, OUTPUT);
	pinMode(IN2, OUTPUT);
	pinMode(IN3, OUTPUT);
	pinMode(IN4, OUTPUT);
        // SRO4
        pinMode(TRIGPIN,OUTPUT);
        pinMode(ECHOPIN,INPUT);
        // Den xe
        pinMode(LEDPIN,OUTPUT);
}
 
 
void motor_1_Tien(int speed) { //speed: từ 0 - MAX_SPEED
	speed = constrain(speed, MIN_SPEED, MAX_SPEED);//đảm báo giá trị nằm trong một khoảng từ 0 - MAX_SPEED - http://arduino.vn/reference/constrain
	digitalWrite(IN1, HIGH);// chân này không có PWM
	analogWrite(IN2, 255 - speed);
}
void motor_1_Lui(int speed) {
	speed = constrain(speed, MIN_SPEED, MAX_SPEED);//đảm báo giá trị nằm trong một khoảng từ 0 - MAX_SPEED - http://arduino.vn/reference/constrain
	digitalWrite(IN1, LOW);// chân này không có PWM
	analogWrite(IN2, speed);
}
void motor_2_Phai(int speed) { //speed: từ 0 - MAX_SPEED
	speed = constrain(speed, MIN_SPEED, MAX_SPEED);//đảm báo giá trị nằm trong một khoảng từ 0 - MAX_SPEED - http://arduino.vn/reference/constrain
        digitalWrite(IN3, HIGH);// chân này không có PWM	
        analogWrite(IN4, 255-speed);
	
}
void motor_2_Trai(int speed) {
	speed = constrain(speed, MIN_SPEED, MAX_SPEED);//đảm báo giá trị nằm trong một khoảng từ 0 - MAX_SPEED - http://arduino.vn/reference/constrain
	digitalWrite(IN3, LOW);// chân này không có PWM
        analogWrite(IN4, speed);
	
}
void motor_1_Dung() {
	digitalWrite(IN1, LOW);
	digitalWrite(IN2, LOW);
}
void motor_2_Dung() {
	digitalWrite(IN3, LOW);
	digitalWrite(IN4, LOW);
}

int getDistance()
{
  digitalWrite(TRIGPIN,LOW);
  delayMicroseconds(5);
  digitalWrite(TRIGPIN,HIGH);
  delayMicroseconds(10);
  
  long time = pulseIn(ECHOPIN,HIGH);
  //long distance = int(0.017*time);
  return 0.017*time;
}
int getLightLevel()
{
  return analogRead(SENSORPIN);
}

char readDataFromControler()
{
  if(serial.available())
  {
    //Serial.println((char)Serial.read());
    char data = serial.read();
    if(isDigit(data))
    {
      String data1 = (String)data +serial.readString();
      Speedi = data1.toInt();
      Serial.println(data1);
      return '0';
    }
    else{
      return data;
    }
  }
  else
    return '0';
}

void loop()
{
  delay(100);
  char dataFromControler = readDataFromControler();
   switch(CONTROL_STATUS)
   {
     case CONTROL_STATUS_A:
       automaticRun();
       if(dataFromControler == 'D')
       {
         GO_STATUS = MOVE_STATUS_STOP;
         CONTROL_STATUS = CONTROL_STATUS_D;
         Serial.println(CONTROL_STATUS);
       }
       break;
     case CONTROL_STATUS_D:
       deviceControl(dataFromControler);
       deviceControlRun();
       if(dataFromControler == 'A')
       {
         GO_STATUS = MOVE_STATUS_GO;
         CONTROL_STATUS = CONTROL_STATUS_A;
         Serial.println(CONTROL_STATUS);
       }
       break;
   }
}

void automaticRun()
{
  int distance = getDistance();
  // Kiem tra mo den xe neu xe di vao vung toi;
  if(getLightLevel()<15)
    digitalWrite(LEDPIN,HIGH);
  else
    digitalWrite(LEDPIN,LOW);
  switch(GO_STATUS)
  {
    case MOVE_STATUS_GO:
       motor_2_Dung();
       motor_1_Tien(getSpeed(distance));
       if(distance < 10)
       {
         int cc = millis()/1000;
         if(cc%30 == 0)
           GO_STATUS = MOVE_STATUS_BARI;
         else
           GO_STATUS = MOVE_STATUS_BALE;
       }
       break;
    case MOVE_STATUS_BA:
      motor_2_Dung();
      motor_1_Lui(getSpeed(distance));
      if(distance > 50)
       {
         int cc = millis()/1000;
         if(cc%2 == 0)
           GO_STATUS = MOVE_STATUS_GO;
         else
           GO_STATUS = MOVE_STATUS_BALE;
       }
      break;
    case MOVE_STATUS_GORI:
      motor_1_Tien(getSpeed(distance)+100);
      motor_2_Phai(MAX_SPEED);
      if(distance < 10)
      {
         motor_2_Dung();
         GO_STATUS = MOVE_STATUS_BALE;
      }
      else if(distance > 50)
      {
        motor_2_Dung();
        GO_STATUS = MOVE_STATUS_GO;
      }
      break;
    case MOVE_STATUS_GOLE:
      motor_1_Tien(getSpeed(distance)+100);
      motor_2_Trai(MAX_SPEED);
      if(distance < 10)
      {
         motor_2_Dung();
         GO_STATUS = MOVE_STATUS_BARI;
      }
      else if(distance > 50)
      {
        motor_2_Dung();
        GO_STATUS = MOVE_STATUS_GO;
      }
      break;
    case MOVE_STATUS_BARI:
      motor_1_Lui(getSpeed(distance)+120);
      motor_2_Phai(MAX_SPEED);
      if(distance > 20);
      {
        motor_2_Dung();
        GO_STATUS = MOVE_STATUS_GOLE;
      }
      break;
    case MOVE_STATUS_BALE:
      motor_1_Lui(getSpeed(distance)+120);
      motor_2_Trai(MAX_SPEED);
      if(distance>20)
      {
        motor_2_Dung();
        GO_STATUS = MOVE_STATUS_GORI;
      }
      break;
    case MOVE_STATUS_STOP:
      carStop();
      break;
  }
  Serial.println(GO_STATUS);
}

int getSpeed(int distance)
{
       if(distance>200)
         return MAX_SPEED;
       else if(distance>150 && distance <=200)
         return MAX_SPEED - 60;
       else if(distance > 100 && distance <=150)
         return MAX_SPEED - 80;
       else if(distance >80 && distance <= 100)
         return MAX_SPEED - 100;
       else if(distance >50 && distance <= 80)
         return MAX_SPEED - 120;
       else if(distance >30 && distance <= 50)
         return MAX_SPEED - 140;
       else if(distance >20 && distance <= 30)
         return MAX_SPEED - 160;
       else if(distance >10 && distance <= 20)
         return MAX_SPEED - 180;
       else
         return 50;
}
void deviceControlRun()
{
  switch(GO_STATUS)
  {
    case MOVE_STATUS_GO:
       motor_2_Dung();
       motor_1_Tien(Speedi);
      
       break;
    case MOVE_STATUS_BA:
      motor_2_Dung();
      motor_1_Lui(Speedi);
      break;
    case MOVE_STATUS_GORI:
      motor_1_Tien(Speedi);
      motor_2_Phai(MAX_SPEED);
      break;
    case MOVE_STATUS_GOLE:
      motor_1_Tien(Speedi);
      motor_2_Trai(MAX_SPEED);
      break;
    case MOVE_STATUS_BARI:
      motor_1_Lui(Speedi);
      motor_2_Phai(MAX_SPEED);
      break;
    case MOVE_STATUS_BALE:
      motor_1_Lui(Speedi);
      motor_2_Trai(MAX_SPEED);
      break;
    case MOVE_STATUS_STOP:
      carStop();
      break;
  }
}
void deviceControl(char dataFromControler)
{
  switch(dataFromControler)
  {
    case 'G':
      GO_STATUS = MOVE_STATUS_GO;
      break;
    case 'B':
      GO_STATUS = MOVE_STATUS_BA;
      break;
    case 'R':
      GO_STATUS = MOVE_STATUS_GORI;
      break;
    case 'L':
      GO_STATUS = MOVE_STATUS_GOLE;
      break;
    case 'W':
      GO_STATUS = MOVE_STATUS_BARI;
      break;
    case 'E':
      GO_STATUS = MOVE_STATUS_BALE;
      break;
    case 'T':
      GO_STATUS = MOVE_STATUS_STOP;
  }
  Serial.println(GO_STATUS);
}
void carStop()
{
  motor_1_Dung();
  motor_2_Dung();
}
